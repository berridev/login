import 'package:flutter/material.dart';

class CustomIcon {
  static const IconData twitter = IconData(0xe900, fontFamily: "CustomIcons");
  static const IconData facebook = IconData(0xe901, fontFamily: "CustomIcons");
  static const IconData instagram = IconData(0xe902, fontFamily: "CustomIcons");
  static const IconData youtube = IconData(0xe903, fontFamily: "CustomIcons");
}
